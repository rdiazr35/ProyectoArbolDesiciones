package proyectoarbolgenerico.classes;

public class DoubleLinkedLists {
    
    private NodeUser first;
    //
    public DoubleLinkedLists(String empty){
        
    }
    //
    public DoubleLinkedLists() {
        this.first = null;
    }
    //
    public void insertStart(User oUser){
        if(this.first == null){
            this.first = new NodeUser(oUser);
        }
        else {
            NodeUser temp = new NodeUser(oUser);
            temp.setNext(this.first);
            this.first.setPrevious(temp);
            this.first = temp;
        }
    }
    //
    public void insertEnd(User oUser){
        if(this.first == null){
            this.first = new NodeUser(oUser);
        }
        else {
            NodeUser temp = this.first;
            while(temp.getNext()!= null){
                temp = temp.getNext();
            }
            NodeUser nn = new NodeUser(oUser);
            temp.setNext(nn);
            nn.setPrevious(temp);
        }
    }
    //
    public String printList(){
        String text = "";
        if(this.first == null){
            text = "Empty List.";
        }
        else {
            NodeUser temp = this.first;
            while(temp.getNext()!= null){
                text += temp.getoUser().getUserName() + ",";
                temp = temp.getNext();
            }
            System.out.println(temp.getoUser().getUserName());
            while(temp != null){
                text += temp.getoUser().getUserName() + ",";
                temp = temp.getPrevious();
            }
        }
        return text;
    }
    //
    public boolean Access(String username, String password){
        boolean access = false;
        if(this.first == null){
            access = false;
        }
        else {
            NodeUser temp = this.first;
            while(temp.getNext()!= null){
                if (username.equals(temp.getoUser().getUserName()) 
                        && password.equals(temp.getoUser().getPassword())){
                    Global.setNombre(temp.getoUser().getFirstName());
                    Global.setUser(temp.getoUser().getUserName());
                    return true;
                }
                temp = temp.getNext();
            }
            while(temp != null){
                if (username.equals(temp.getoUser().getUserName()) 
                        && password.equals(temp.getoUser().getPassword())){
                    Global.setNombre(temp.getoUser().getFirstName());
                    Global.setUser(temp.getoUser().getUserName());
                    return true;
                }
                temp = temp.getPrevious();
            }
        }
        return access;
    }
    //
}//END CLASS