package proyectoarbolgenerico.classes;

import java.util.ArrayList;
import org.w3c.dom.Document;

public class Global {

    private static ArrayList<User> lstUser = new ArrayList<>();
    private static DoubleLinkedLists doubleLinkedList;
    private static String name;
    private static String user;
    private static String root;
    private static Document doc = null;
    private static NodeTree oNode;

    public static ArrayList<User> getLstUser() {
        return lstUser;
    }
    //
    public static void setLstUser(ArrayList<User> aLstUser) {
        lstUser = aLstUser;
    }
    //
    public static String getName() {
        return name;
    }
    //
    public static void setNombre(String aName) {
        name = aName;
    }
    //
    public static String getUser() {
        return user;
    }
    //
    public static void setUser(String aUser) {
        user = aUser;
    }
    //
    public static DoubleLinkedLists getDoubleLinkedList() {
        return doubleLinkedList;
    }
    //
    public static void setDoubleLinkedList(DoubleLinkedLists aDoubleLinkedList) {
        doubleLinkedList = aDoubleLinkedList;
    }
    //
    public static String getRoot() {
        return root;
    }
    //
    public static void setRoot(String aRoot) {
        root = aRoot;
    }
    //
    public static Document getDoc() {
        return doc;
    }
    //
    public static void setDoc(Document aDoc) {
        doc = aDoc;
    }
    //
    public static NodeTree getoNode() {
        return oNode;
    }
    //
    public static void setoNode(NodeTree aoNode) {
        oNode = aoNode;
    }
    //
}//END 