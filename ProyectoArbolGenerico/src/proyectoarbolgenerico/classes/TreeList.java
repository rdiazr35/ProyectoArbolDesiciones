package proyectoarbolgenerico.classes;

public class TreeList{
    
    private static NodeTree first = null;

    public TreeList() {
    }
    //
    public void insert(String root, TreeList oTreeList){
        if(TreeList.first == null){
            TreeList.first = new NodeTree(root, oTreeList);
        }
        else {
            NodeTree temp = new NodeTree(root, oTreeList);
            temp.setSig(TreeList.first);
            TreeList.first.setAnt(temp);
            TreeList.first = temp;
        }
        if(TreeList.first != null){
            Global.setoNode(first);
        }
    }
    //
    public String printRoot(){
        if(TreeList.first == null){
            System.out.println("Lista Vacía.");
            return "";
        }
        else {
            String result = "";
            NodeTree temp = TreeList.first;
            while(temp.getSig() != null){
                result += temp.getRoot()+ ",";
                temp = temp.getSig();
            }
            result += temp.getRoot()+ ",";            
            while(temp != null){
                result += temp.getRoot()+ ",";
                temp = temp.getAnt();
            }
            result = result.replace(",","");
            return result;
        }
    }
    
}//