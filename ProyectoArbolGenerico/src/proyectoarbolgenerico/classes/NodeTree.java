package proyectoarbolgenerico.classes;

public class NodeTree {
    
    private String root;
    private TreeList oTreeList;
    private NodeTree ant;
    private NodeTree sig;

    public NodeTree(String root, TreeList oTreeList) {
        this.root = root;
        this.oTreeList = oTreeList;
        this.ant = null;
        this.sig = null;
    }

    public NodeTree() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    //
    public String getRoot() {
        return root;
    }
    //
    public void setRoot(String root) {
        this.root = root;
    }
    //
    public NodeTree getAnt() {
        return ant;
    }
    //
    public void setAnt(NodeTree ant) {
        this.ant = ant;
    }
    //
    public NodeTree getSig() {
        return sig;
    }
    //
    public void setSig(NodeTree sig) {
        this.sig = sig;
    }
    //
    public TreeList getoTreeList() {
        return oTreeList;
    }
    //
    public void setoTreeList(TreeList oTreeList) {
        this.oTreeList = oTreeList;
    }
    //
}
