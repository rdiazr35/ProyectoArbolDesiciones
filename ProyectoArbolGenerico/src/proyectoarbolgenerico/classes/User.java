package proyectoarbolgenerico.classes;

import java.util.ArrayList;

public class User {
    
    private String firstName = "";
    private String lastName = "";
    private String userName = "";
    private String password = "";
    private String birthdate = "";
    private String address = "";
    
    //Empty Constructor
    public User(){
        
    }
    //Overloaded Constructor
    public User(String _firstName, String _lastName, String _user, String _password,
            String _birthdate, String _address){
        this.firstName = _firstName;
        this.lastName = _lastName;
        this.userName = _user;
        this.password = _password;
        this.birthdate = _birthdate;
        this.address = _address;
    }
    //
    public String getPassword() {
        return password;
    }
    //
    public void setPassword(String _password) {
        this.password = _password;
    }
    //
    public String getFirstName() {
        return firstName;
    }
    //
    public void setFirstName(String _firstName) {
        this.firstName = _firstName;
    }
    //
    public String getLastName() {
        return lastName;
    }
    //
    public void setLastName(String _lastName) {
        this.lastName = _lastName;
    }
    //
    public String getUserName() {
        return userName;
    }
    //
    public void setUserName(String _userName) {
        this.userName = _userName;
    }
    //
    public String getBirthDate() {
        return birthdate;
    }
    //
    public void setBirthDate(String _birthdate) {
        this.birthdate = _birthdate;
    }
    //
    public String getAddress() {
        return address;
    }
    //
    public void setAddress(String _address) {
        this.address = _address;
    }
    //
        
}//END