package proyectoarbolgenerico.classes;

public class NodeUser {
    //
    private NodeUser next;
    private NodeUser previous;
    private User oUser;
    //
    public NodeUser(){
        this.oUser = null;
        this.next = null;
        this.previous = null;
    }
    //
    public NodeUser(User _oUser){
        this.oUser = _oUser;
        this.next = null;
        this.previous = null;
    }
    //
    public NodeUser getNext() {
        return next;
    }
    //
    public void setNext(NodeUser next) {
        this.next = next;
    }
    //
    public NodeUser getPrevious() {
        return previous;
    }
    //
    public void setPrevious(NodeUser previous) {
        this.previous = previous;
    }
    //
    public User getoUser() {
        return oUser;
    }
    //
    public void setoUser(User oUser) {
        this.oUser = oUser;
    }
    //
    
}//END CLASS